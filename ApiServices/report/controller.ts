import { Handler } from "express";
import { PrismaClient } from '@prisma/client'

// Instantiate Prisma Client
const prisma = new PrismaClient()

export const getReports: Handler = async (req, res) => {
  try {

  //console.log(req.session.user);
    const report = await prisma.report.findMany({
    });
    return res.json(report)
  } catch (e) {
    console.log(e); 
    return res.status(500).json('Internal Server error');
  }
  
}



export const postReport: Handler = async (req , res) => {
  try {
    const reportData = req.body;
    console.log(reportData);
    await prisma.report.create({
      data: {
        ...reportData
      }
    });
    return res.json(reportData);
  } catch (error) {
    console.log(error)
    return res.status(500).json('Internal Server error');
  }
  
};


export const updateReport: Handler = async (req, res) => {
  try {
    const { release,  comment , jsonReport, app } = req.body;
    const { id } = req.params;

    const updatedReport = await prisma.report.update({
      where: {
        id:  id 
      },
      data: {
        release,  jsonReport, app, comment ,
      }
    });

    return res.json(updatedReport)
  } catch (error) {
    console.log(error)
    return res.status(500).json('Internal Server error');
  }
  
}

export const deleteReport: Handler = async (req, res) => {
  try {
    const { id } = req.params;
    const deletedReport = await prisma.report.delete({
      where: {
        id:  id 
      }
    });
    return res.json( deletedReport );

  } catch (error) {
    console.log(error)
    return res.status(500).json('Internal Server error');
  }
  
}

import {Router} from 'express';
const reportRouter = Router();
import { getReports,postReport, updateReport, deleteReport } from './controller';


reportRouter.get('/',getReports);
reportRouter.post('/',postReport);
reportRouter.put('/:id',updateReport);
reportRouter.delete('/:id',deleteReport);

export default reportRouter;

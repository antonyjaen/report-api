import Router from 'express'
import  reportRouter  from '../ApiServices/report/routes';

const router = Router();

//,authUser("users"),

//todo apply middleware to all routes with session auth 
router.use('/report', reportRouter); 


export default router;
